This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to run

In the project directory, run:

### `npm install`

then

### `npm run build`

then

### `npm start`

Then go to localhost:5000 to view the page.
