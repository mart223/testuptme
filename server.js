const express = require("express")
const path = require("path")
const request = require("request")

const {promisify} = require("util")

const Mercury = require("@postlight/mercury-parser")

const rp = promisify(request)

let Parser = require("rss-parser")
let parser = new Parser()

const PORT = process.env.PORT || 5000
const app = express()
app.use(express.static(path.join(__dirname, "build")))

let feedCache = {data: null, date: new Date()}

app.get("/api/feed", async (req, res) => {
    
    // This simple cache makes it easier to test
    const cacheTime = 1000 * 60 * 60
    
    if (feedCache.data && cacheTime > new Date().getTime() - feedCache.date.getTime()) {
        
        res.json(feedCache.data)
        
        return
        
    }
    
    const url = "https://flipboard.com/@raimoseero/feed-nii8kd0sz.rss"
    const feed = await parser.parseURL(url)
    
    // Retrieving the feed contents right away because there do not appear to be any image URL-s in the feed itself
    // but the task appears to really want images on the feed buttons
    
    // An improvement might be to implement a lazy load mechanism to retrieve the cleanContents data
    
    // Retrieving the contents manually because then it is possible to set "rejectUnauthorized: false"
    
    let items = feed.items
    const feedContents = (await Promise.all(items.map((item) =>
        
        rp({
            url: item.link,
            // Had to use this option as I kept getting Error: certificate has expired
            //     at TLSSocket.onConnectSecure
            // A perhaps better way to handle this would be to just not display the data for those pages that have the
            // issue
            agentOptions: {
                rejectUnauthorized: false
            }
        }).catch((error) => {
            
            console.log(error)
            
        })
    ))).map(value => value.body)
    
    // if (error || response.statusCode !== 200) {
    //     return response.status(500).json({type: "error", message: error.message})
    // }
    
    const feedContentObjects = await Promise.all(feedContents.map((itemContents, index, array) =>
        
        Mercury.parse(items[index].link, {
            html: itemContents
        }).catch((error) => {
            
            console.log(error)
            
            console.log(itemContents)
            
        })
    ))
    
    feedContentObjects.forEach(
        (result, index) => items[index].cleanContents = result
    )
    
    feedCache = {data: feed, date: new Date()}
    
    feed.updatedAt = feedCache.date
    
    res.json(feed)
    
})

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, "build", "index.html"))
})

app.listen(PORT)
