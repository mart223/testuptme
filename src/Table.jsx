import React, {useEffect, useState} from "react"
import {TableItem} from "./TableItem"


export function Table(props) {
    
    const [data, setData] = useState(null)
    
    useEffect(() => {
        
        const request = new XMLHttpRequest()
        request.onreadystatechange = () => {
            
            if (request.readyState === 4 && request.status === 200) {
                
                setData(JSON.parse(request.responseText))
                
            }
            
        }
        request.open("GET", "/api/feed", true)
        request.send()
        
    }, [])
    
    return (
        
        [
            <div>
                
                {data && <h2> {data.title} </h2>}
                {data && <h5> Retrieved on {data.updatedAt} (updated hourly) </h5>}
            
            </div>,
            <div className={"Table"}>
                
                {!data && <h2> Loading (Can take a while) </h2>}
                
                {data && data.items.map(item => <TableItem item={item}/>)}
            
            </div>
        ]
    )
    
}










