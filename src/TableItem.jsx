import React, {useCallback} from "react"


export function TableItem({item}) {
    
    const openItem = useCallback((event) => {
        
        const itemWindow = window.open(
            "",
            item.cleanContents.title,
            "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes"
        )
        
        itemWindow.document.body.innerHTML = item.cleanContents.content
        
    }, [item])
    
    
    return (
        <div className={"Table-item"} onClick={openItem}>
            
            <img src={item.cleanContents.lead_image_url || "logo192.png"}
                 alt={"Article"}/>
            
            <h3> {item.title} </h3>
            <p> {item.content} </p>
    
            <label> Publication date {item.pubDate} </label>
        
        </div>
    )
    
}










